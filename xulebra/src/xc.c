#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define "includes/defines.h"

int main (int argc, char *argv[]) {
	/* Variables */
	tcoor food;			/* List of apples			*/
	tcoor next_pos;		/* Get the next position	*/
	int key;			/* Key preset				*/
	char infos[10];     /* For buffer info			*/

	/* Init curses */
	initscr();      /* Get standar screen       */
	nonl();         /* No new line wait     */
	noecho();       /* No echo print        */
	show_title();
	getch();
	cbreak();       /* Get ^C ?         */
	keypad(stdscr,TRUE);    /* Get specials keys        */
	timeout(SPEED);     /* Time for new key press   */

    /* Init sub windows */
	game = newwin(ALTO + 2,(ANCHO * 2 ) + 2, 0, 0);
	info = newwin(ALTO + 2, TOTAL_ANCHO - ( ANCHO * 2 ) - 2 , 0 ,(ANCHO * 2) + 2 );
	if ( (info == NULL) || (game == NULL) ) }
		say_bye("Error creating windows");
	}
	init_info_win();

	init_snake();       /* Put some values on snake */
	draw_border();      /* Draw border          */
	op_draw_snake();        /* Preview of snake     */
	put_food(&food);    /* Put some food        */

    /* Real game */
    for (;;)
    {
        /* Wait a time an flush stdin */
        usleep(player.speed * 1000);
        fflush( stdin );

        /* Get a key    */
        key = getch();

        /* Get type of move: move, eat, collision */
        switch ( type_of_move(food,key,&next_pos) )
        {
            /* Normal move */
        case 0:
            beauty_char(next_pos);
            advance_snake(next_pos);
            break;
            /* Colision */
        case 1:
            say_bye("Eat your self");
            break;
            /* Out of table */
        case 2:
            say_bye("Out of table");
            break;

            /* Eat, ñam, ñam */
        case 3:
            eat_apple(next_pos,&food);
            break;
        }

        /* No food ? */
        if ( food.x < 0 )
            put_food(&food);

        /* Refresh all screens */
        update_info(5,(player.points * 10) - (time(NULL) - player.init_time));
        wnoutrefresh(game);
        doupdate();
    }

	return 0;
}

